package mBuku

import (
	"database/sql"
	"fmt"
	"time"
)

type Buku struct {
	Id        int64 `json:"id,string"`
	Judul     string
	Pengarang string
	Row       string
	Rak       string
	Status    string
	UpdatedAt time.Time
}

const dateFormat = `2006-01-02 15:04:05`

func Find(db *sql.DB, k *string) (bukus []Buku, err error) {
	rows, err := db.Query(`SELECT * FROM buku WHERE judul_buku LIKE ?`, k)
	bukus = []Buku{}
	if err != nil {
		return bukus, err
	}
	defer rows.Close()
	for rows.Next() {
		s := Buku{}
		updatedAt := ``
		err = rows.Scan(
			&s.Id,
			&s.Judul,
			&s.Pengarang,
			&s.Row,
			&s.Rak,
			&s.Status,
			&updatedAt)
		if err != nil {
			return
		}
		s.UpdatedAt, _ = time.Parse(dateFormat, updatedAt)
		bukus = append(bukus, s)
	}
	return
}

func Insert(db *sql.DB, m *Buku) (err error) {
	now := time.Now()
	res, err := db.Exec(`INSERT INTO buku(judul_buku,pengarang_buku,row,rak,status,updatedAt)
VALUES(?,?,?,?,?,?)`,
		m.Judul,
		m.Pengarang,
		m.Row,
		m.Rak,
		m.Status,
		now)
	if err != nil {
		return err
	}
	m.Id, err = res.LastInsertId()
	if err == nil {
		m.UpdatedAt = now
	}
	return err
}

func Update(db *sql.DB, m *Buku) (err error) {
	keys := []string{}
	values := []interface{}{}
	if m.Judul != `` {
		keys = append(keys, `judul_buku`)
		values = append(values, m.Judul)
	}
	if m.Pengarang != `` {
		keys = append(keys, `pengarang_buku`)
		values = append(values, m.Pengarang)
	}
	if m.Row != `` {
		keys = append(keys, `row`)
		values = append(values, m.Row)
	}
	if m.Rak != `` {
		keys = append(keys, `rak`)
		values = append(values, m.Rak)
	}
	if m.Status != `` {
		keys = append(keys, `status`)
		values = append(values, m.Status)
	}
	sql := `UPDATE buku SET `
	for idx, key := range keys {
		if idx > 0 {
			sql += `,`
		}
		sql += key + `= ?`
	}
	sql += ` ,updatedAt = ? WHERE id = ?`
	now := time.Now()
	values = append(values, now, m.Id)
	res, err := db.Exec(sql, values...)
	if err != nil {
		return err
	}
	check, err := res.RowsAffected()
	fmt.Println(check)
	if err == nil {
		m.UpdatedAt = now
	}
	return err
}
