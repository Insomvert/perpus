package main

import (
	"buku/handler"
	"buku/handler/hGuest"
	"buku/handler/hStaff"
)

const PORT = `:7000`

func main() {
	server := handler.InitServer(`views/`)
	server.Handle(`/staff/input`, hStaff.BookCreate)
	server.Handle(`/staff/update`, hStaff.BookUpdate)
	server.Handle(`/guest/find`, hGuest.FindBook)
	server.Listen(PORT)
}
