package config

import (
	"database/sql"
	"fmt"

	_ "github.com/go-sql-driver/mysql"
)

const dsn = `perpus:1234567@/perpus`

func ConnectMysql() (db *sql.DB, err error) {
	db, err = sql.Open(`mysql`, dsn)
	if err != nil {
		fmt.Println(err)
	}
	return
}
