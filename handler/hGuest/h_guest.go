package hGuest

import (
	"buku/handler"
	"buku/model/mBuku"
	"net/url"
)

func FindBook(ctx *handler.Ctx) {
	keyword := ctx.Request.URL.Query().Get("s")
	key, err := url.QueryUnescape(keyword)
	s := "%" + key + "%"
	bukus, err := mBuku.Find(ctx.Db, &s)
	if ctx.IsError(err) {
		return
	}
	ctx.End(bukus)
}
