package hStaff

import (
	"buku/handler"
	"buku/model/mBuku"
	"encoding/json"
	"net/http"
)

func BookCreate(ctx *handler.Ctx) {
	if ctx.Request.Method == `GET` {
		http.ServeFile(ctx, ctx.Request, ctx.ViewsDir+`staff/input_buku.html`)
		return
	}
	m := mBuku.Buku{}
	err := json.NewDecoder(ctx.Request.Body).Decode(&m)
	if ctx.IsError(err) {
		return
	}
	err = mBuku.Insert(ctx.Db, &m)
	if ctx.IsError(err) {
		return
	}
	ctx.End(m)
}

func BookUpdate(ctx *handler.Ctx) {
	if ctx.Request.Method == `GET` {
		http.ServeFile(ctx, ctx.Request, ctx.ViewsDir+`staff/update_buku.html`)
		return
	}
	m := mBuku.Buku{}
	err := json.NewDecoder(ctx.Request.Body).Decode(&m)
	if ctx.IsError(err) {
		return
	}
	err = mBuku.Update(ctx.Db, &m)
	if ctx.IsError(err) {
		return
	}
	ctx.End(m)
}
